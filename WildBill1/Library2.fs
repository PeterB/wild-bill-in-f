﻿module WildBill2

open WildBill1

let private cutoff amount original : double =
    max 0 (original - amount) 
    |> double

let private minutescost plan minutes =
    let excess = max 0 (minutes - 1000)
    match plan with
    | Gold -> cutoff 1000 minutes |> (*) 0.45
    | Silver ->  cutoff 500 minutes |> (*) 0.54


type Account with
    member internal this.minuteComponent =
         (minutescost this.plan this.minutes)
    member this.bill2 =
        this.bill1 + this.minuteComponent