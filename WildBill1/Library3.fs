﻿module WildBill3

open WildBill1
open WildBill2

type Account with
    member private this.firstAdditionalLines = 
        min 2 (this.lines - 1)

    member private this.familyLines = 
        max 0 (this.lines - this.firstAdditionalLines - 1)

    member internal this.familyComponent =
        double(this.familyLines) * 5.00

    member internal this.firstLinesComponent =
        double(this.firstAdditionalLines) * this.linecost

    member this.bill3 =
        this.baseComponent + this.familyComponent + this.firstLinesComponent + this.minuteComponent 
