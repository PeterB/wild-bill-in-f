﻿module WildBill1

type Plan = Silver | Gold

type Account = { plan:Plan; lines:int; minutes: int; }


type Account with
    member internal this.linecost =
        match this.plan with
        | Gold -> 14.50
        | Silver -> 21.50

    member internal this.baseComponent =
        match this.plan with
        | Gold -> 49.95
        | Silver -> 29.95

    member internal this.lineComponent = 
        double(this.lines-1) * this.linecost

    member this.bill1 =
        this.baseComponent + this.lineComponent