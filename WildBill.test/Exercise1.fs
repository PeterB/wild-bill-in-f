﻿namespace WildBill.test1
open NUnit.Framework
open WildBill1

type Exercise() =
    member internal this.AssertValue (expected:double) (actual:double) : unit =
        Assert.AreEqual(expected, actual, 0.01) 

[<TestFixture>]
type Exercise1() = 
    inherit Exercise()
    let onecent = 0.01

    [<Test>]
    member this.``Gold Plan with 1 line is 49.95``() =
        let cost = { plan=Gold; lines=1; minutes=0 }.bill1
        this.AssertValue 49.95 cost

    [<Test>]
    member this.``Gold Plan with 2 lines is 64.45``() =
        let cost = { plan=Gold; lines=2; minutes=0 }.bill1
        this.AssertValue 64.45 cost

    [<Test>]
    member this.``Silver Plan with 1 lines is 29.95``() =
        let cost = { plan=Silver; lines=1; minutes=0 }.bill1
        this.AssertValue 29.95 cost

    [<Test>]
    member this.``Silver Plan with 3 lines is 72.95``() =
        let cost = { plan=Silver; lines=3; minutes=0 }.bill1
        this.AssertValue 72.95 cost
