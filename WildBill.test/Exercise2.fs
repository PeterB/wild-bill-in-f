﻿namespace WildBill.test2
open NUnit.Framework
open WildBill1
open WildBill2
open WildBill.test1

[<TestFixture>]
type Exercise2() = 
    inherit Exercise()

    [<Test>]
    member this.``Gold Plan with 999 minutes is 49.95``() =
        let cost = { plan = Gold; lines = 1; minutes = 999 }.bill2
        this.AssertValue 49.95 cost

    [<Test>]
    member this.``Gold Plan with 1000 minutes is 49.95``() =
        let cost = { plan = Gold; lines = 1; minutes = 1000 }.bill2
        this.AssertValue 49.95 cost

    [<Test>]
    member this.``Gold Plan with 1001 minutes is 50.40``() =
        let cost = { plan = Gold; lines = 1; minutes = 1001 }.bill2
        this.AssertValue 50.40 cost

    [<Test>]
    member this.``Gold Plan with 1010 minutes is 54.45``() =
        let cost = { plan = Gold; lines = 1; minutes = 1010 }.bill2
        this.AssertValue 54.45 cost

    [<Test>]
    member this.``Silver Plan with 499 minutes is 29.95``() =
        let cost = { plan = Silver; lines = 1; minutes = 499 }.bill2
        this.AssertValue 29.95 cost

    [<Test>]
    member this.``Silver Plan with 500 minutes is 29.95``() =
        let cost = { plan = Silver; lines = 1; minutes = 500 }.bill2
        this.AssertValue 29.95 cost

    [<Test>]
    member this.``Silver Plan with 520 minutes is 40.75``() =
        let cost = { plan = Silver; lines = 1; minutes = 520 }.bill2
        this.AssertValue 40.75 cost
