﻿namespace WildBill.test3
open NUnit.Framework
open WildBill1
open WildBill2
open WildBill3
open WildBill.test1
open WildBill.test2

[<TestFixture>]
type Exercise3() = 
    inherit Exercise()

    [<Test>]
    member this.``Gold Plan with 2 lines is still 64.45``() =
        let cost = { plan = Gold; lines = 2; minutes = 0 }.bill3
        this.AssertValue 64.45 cost

    [<Test>]
    member this.``Gold Plan with 3 lines is still 78.95``() =
        let cost = { plan = Gold; lines = 3; minutes = 0 }.bill3
        this.AssertValue 78.95 cost

    [<Test>]
    member this.``Gold Plan with 4 lines is 83.95``() =
        let cost = { plan = Gold; lines = 4; minutes = 0 }.bill3
        this.AssertValue 83.95 cost
