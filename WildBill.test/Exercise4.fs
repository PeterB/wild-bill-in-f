﻿namespace WildBill.test3
open NUnit.Framework
open WildBill1
open WildBill2
open WildBill3
open WildBill.test1
open WildBill.test2
open WildBill.test3

[<TestFixture>]
type Exercise4() = 
    inherit Exercise()

    [<Test>]
    member this.``Scenario tests``() =
        this.AssertValue  83.95 ({plan=Gold;   minutes=878;  lines=4 }.bill3)
        this.AssertValue 105.30 ({plan=Gold;   minutes=1123; lines=1 }.bill3)
        this.AssertValue 139.30 ({plan=Gold;   minutes=1123; lines=4 }.bill3)
        this.AssertValue  63.87 ({plan=Silver; minutes=523;  lines=2 }.bill3)
        this.AssertValue  82.95 ({plan=Silver; minutes=44;   lines=5 }.bill3)
        this.AssertValue  94.29 ({plan=Silver; minutes=521;  lines=5 }.bill3)

